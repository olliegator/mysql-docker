Build:

docker build . -t amtal/mysql-docker

Run:

docker run -e MYSQL_ROOT_PASSWORD=user -e MYSQL_DATABASE=user -e MYSQL_USER=user -e MYSQL_PASSWORD=user -p 3306:3306 --name mysql amtal/mysql-docker
