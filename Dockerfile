FROM mysql:8

COPY max-connections.cnf /etc/mysql/mysql.conf.d

HEALTHCHECK --interval=30s --timeout=30s --start-period=5s --retries=3 CMD /usr/bin/mysqladmin ping --silent
